//Script for dynamically date updation
var d = new Date();
//Using array to convert numeric value of month into string
var Day = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
var Month = ["January", "February", "March", "April", "May", "june", "July", "August", "September",
    "October", "November", "December"];
document.getElementById("dt").innerHTML = d.getDate() + " " + Month[d.getMonth()] + " " + d.getFullYear() + " " + "Last Updated at " + d.getHours() + ":"+ d.getMinutes();
document.getElementById("year").innerHTML = "Copyright © " + " " + d.getFullYear() + " " + "BBC.";